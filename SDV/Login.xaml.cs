﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SDV
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            txtUser.Focus();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
        public string Username { get; set; }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            EF.DBModel db = new EF.DBModel();

            string pass = GetHashString(txtPass.Password);
            var users = db.Operador.Where(p => p.Usuario == txtUser.Text && p.Pass == pass && p.OperadorSDV == true);
            if (users.Count() != 0) 
            {
                Username = txtUser.Text;
                DialogResult = true;
                Util.Sounds.Welcome.Play();
            }
            else
            {
                BeginStoryboard(FindResource("LoginFailedAnimation") as Storyboard);
                Util.Sounds.Asterisk.Play();
            }
        }
        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = MD5.Create();  //or use SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
