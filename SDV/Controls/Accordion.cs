﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Controls
{
    [StyleTypedProperty(Property = "ItemContainerStyle", StyleTargetType = typeof(AccordionItem))]
    public class Accordion : ItemsControl
    {
        static Accordion()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Accordion), new FrameworkPropertyMetadata(typeof(Accordion)));
        }

        public Accordion(): base()
        {
            ItemContainerGenerator.StatusChanged += (s, a) =>
            {
                if(ItemContainerGenerator.Status == System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated && ItemContainerGenerator.Items.Count > 0)
                {
                    ItemsGenerated?.Invoke(this, null);
                }
            };

        }

        [Bindable(true)]
        public static readonly DependencyProperty ContentTemplateProperty =
            DependencyProperty.Register("ContentTemplate", typeof(DataTemplate), typeof(Accordion),
            new PropertyMetadata((s, e) => ((Accordion)s).UpdateItems()));

        [Bindable(true)]
        public DataTemplate ContentTemplate
        {
            get { return (DataTemplate)GetValue(ContentTemplateProperty); }
            set { SetValue(ContentTemplateProperty, value); }
        }


        public static readonly DependencyProperty ContentTemplateSelectorProperty = DependencyProperty.Register("ContentTemplateSelector", typeof(DataTemplateSelector), typeof(Accordion), new FrameworkPropertyMetadata((DataTemplateSelector)null));

        /// <summary>
        ///     ContentTemplateSelector allows the app writer to provide custom style selection logic.
        /// </summary>
        public DataTemplateSelector ContentTemplateSelector
        {
            get
            {
                return (DataTemplateSelector)GetValue(ContentTemplateSelectorProperty);
            }
            set
            {
                SetValue(ContentTemplateSelectorProperty, value);
            }
        }

        /// <summary>
        ///     The DependencyProperty for the ContentStringFormat property.
        ///     Flags:              None
        ///     Default Value:      null
        /// </summary>
        public static readonly DependencyProperty ContentStringFormatProperty =
                DependencyProperty.Register(
                        "ContentStringFormat",
                        typeof(String),
                        typeof(Accordion),
                        new FrameworkPropertyMetadata((String)null));


        /// <summary>
        ///     ContentStringFormat is the format used to display the content of
        ///     the control as a string.  This arises only when no template is
        ///     available.
        /// </summary>
        public String ContentStringFormat
        {
            get { return (String)GetValue(ContentStringFormatProperty); }
            set { SetValue(ContentStringFormatProperty, value); }
        }


        protected override DependencyObject GetContainerForItemOverride()
        {
            return new AccordionItem() { ContentTemplate = ContentTemplate };
        }

        protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        {
            base.PrepareContainerForItemOverride(element, item);

            //AccordionItem i = item as AccordionItem;
            //if(i != null)
            //{
            //    i.ContentTemplate = ItemTemplate;
            //    i.HeaderTemplate = ContentTemplate;
            //}
        }

        private void UpdateItems()
        {
            //Actually it is possible to use only the ItemsSource property,
            //but I would rather wait until both properties are set
            if (this.ItemsSource == null || this.ContentTemplate == null)
                return;

            //foreach (var item in this.ItemsSource)
            //{
            //    var visualItem = this.ItemTemplate.LoadContent() as FrameworkElement;
            //    if (visualItem != null)
            //    {
            //        visualItem.DataContext = item;
            //        //Add the visualItem object to a list or StackPanel
            //        //...
            //    }
            //}
        }
        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            base.OnItemsSourceChanged(oldValue, newValue);
            UpdateItems();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is AccordionItem;
        }

        public List<object> SelectedItems
        {
            get
            {
                List<object> l = new List<object>();
                for (int i = 0; i < ItemContainerGenerator.Items.Count; i++)
                {
                    if (ItemContainerGenerator.ContainerFromIndex(i) is AccordionItem)
                    {
                        if (((AccordionItem)ItemContainerGenerator.ContainerFromIndex(i)).IsOpen)
                        {
                            l.Add(ItemContainerGenerator.Items[i]);
                        }
                    }
                }
                return l;
            }
        }

        public void OpenAccordionItem(int index, bool closeOthers = false)
        {
            if (index < 0 || index > ItemContainerGenerator.Items.Count() - 1)
                throw new ArgumentOutOfRangeException();

            if (closeOthers)
            {
                for(int i = 0; i< ItemContainerGenerator.Items.Count; i++)
                    if (ItemContainerGenerator.ContainerFromIndex(i) is AccordionItem)
                        ((AccordionItem)ItemContainerGenerator.ContainerFromIndex(i)).IsOpen = false;
            }

            if (ItemContainerGenerator.ContainerFromIndex(index) is AccordionItem)
                ((AccordionItem)ItemContainerGenerator.ContainerFromIndex(index)).IsOpen = true;
        }

        public void CloseAccordionItem(int index)
        {
            if (index < 0 || index > ItemContainerGenerator.Items.Count() - 1)
                throw new ArgumentOutOfRangeException();



            if (ItemContainerGenerator.ContainerFromIndex(index) is AccordionItem)
                ((AccordionItem)ItemContainerGenerator.ContainerFromIndex(index)).IsOpen = false;
        }

        public void CloseAllAccordionItems()
        {
            for (int i = 0; i < ItemContainerGenerator.Items.Count; i++) 
                if (ItemContainerGenerator.ContainerFromIndex(i) is AccordionItem)
                    ((AccordionItem)ItemContainerGenerator.ContainerFromIndex(i)).IsOpen = false;
        }

        public event EventHandler ItemsGenerated;
    }
    [Localizability(LocalizationCategory.None, Readability = Readability.Unreadable)]
    public class AccordionItem : HeaderedContentControl
    {
        static AccordionItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AccordionItem), new FrameworkPropertyMetadata(typeof(AccordionItem)));
            AutomationProperties.IsOffscreenBehaviorProperty.OverrideMetadata(typeof(AccordionItem), new FrameworkPropertyMetadata(IsOffscreenBehavior.FromClip));
        }
        //public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register("Header", typeof(object),
        //    typeof(AccordionItem), new FrameworkPropertyMetadata());

        //public object Header
        //{
        //    get { return GetValue(HeaderProperty); }
        //    set { SetValue(HeaderProperty, value); }
        //}

        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool),
            typeof(AccordionItem), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | 
                FrameworkPropertyMetadataOptions.AffectsParentMeasure| FrameworkPropertyMetadataOptions.Journal, new PropertyChangedCallback(OnIsOpenChanged)));

        private static void OnIsOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var k = e.NewValue;
        }

        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new FrameworkElementAutomationPeer(this);
        }

        //public static readonly DependencyProperty HeaderTemplateProperty =
        //        DependencyProperty.Register(
        //                "HeaderTemplate",
        //                typeof(DataTemplate),
        //                typeof(AccordionItem),
        //                new FrameworkPropertyMetadata((DataTemplate)null, null));

        //[Bindable(true)]
        //public DataTemplate HeaderTemplate
        //{
        //    get { return (DataTemplate)GetValue(HeaderTemplateProperty); }
        //    set { SetValue(HeaderTemplateProperty, value); }
        //}
    }
}
