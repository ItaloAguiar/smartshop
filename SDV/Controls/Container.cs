﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SDV.Controls
{
    public class Container:ContentControl
    {
        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);
            RaiseContentChangedEvent();
        }
        public static readonly RoutedEvent ContentChangedEvent = EventManager.RegisterRoutedEvent(
        "ContentChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Container));

        // Provide CLR accessors for the event 
        public event RoutedEventHandler ContentChanged
        {
            add { AddHandler(ContentChangedEvent, value); }
            remove { RemoveHandler(ContentChangedEvent, value); }
        }

        // This method raises the Tap event 
        void RaiseContentChangedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(Container.ContentChangedEvent);
            RaiseEvent(newEventArgs);
        }
    }
}
