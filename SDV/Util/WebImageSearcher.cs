﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SDV.Util
{
    public static class WebImageSearcher
    {
        public static byte[] FromText(string value)
        {
            try
            {
                WebClient cli = new WebClient();
                var c = cli.DownloadString($"https://www.googleapis.com/customsearch/v1?q={value}&cx=partner-pub-9359338146946098:8564580287&searchType=image&key=AIzaSyAcadOlS_Cijs7v6PaWbM4CJ3i_3Zd8gXo");

                var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(c);

                var img = cli.DownloadData(obj.items[0].image.thumbnailLink);
                return img;
            }
            catch
            {
                return null;
            }
        }
    }

    internal class Image
    {
        public string contextLink { get; set; }
        public string thumbnailLink { get; set; }
    }

    internal class Item
    {
        public string link { get; set; }
        public string displayLink { get; set; }
        public Image image { get; set; }
    }

    internal class RootObject
    {
        public List<Item> items { get; set; }
    }
}
