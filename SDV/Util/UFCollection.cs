﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SDV.Util
{
    public static class UFCollection
    {
        static UFCollection()
        {
            Dictionary<string, string> uf = new Dictionary<string, string>();


            uf.Add("AC", "Acre");
            uf.Add("AL", "Alagoas");
            uf.Add("AP", "Amapá");
            uf.Add("AM", "Amazonas");
            uf.Add("BA", "Bahia");
            uf.Add("CE", "Ceará");
            uf.Add("DF", "Distrito Federal");
            uf.Add("ES", "Espírito Santo");
            uf.Add("GO", "Goiás");
            uf.Add("MA", "Maranhão");
            uf.Add("MT", "Mato Grosso");
            uf.Add("MS", "Mato Grosso do Sul");
            uf.Add("MG", "Minas Gerais");
            uf.Add("PA", "Pará");
            uf.Add("PB", "Paraíba");
            uf.Add("PE", "Pernambuco");
            uf.Add("PI", "Piauí");
            uf.Add("RJ", "Rio de Janeiro");
            uf.Add("RN", "Rio Grande do Norte");
            uf.Add("RS", "Rio Grande do Sul");
            uf.Add("RO", "Rondônia");
            uf.Add("RR", "Roraima");
            uf.Add("SC", "Santa Catarina");
            uf.Add("SP", "São Paulo");
            uf.Add("SE", "Sergipe");
            uf.Add("TO", "Tocantins");

            Estados = uf;

        }
        public static Dictionary<string,string> Estados { get; set; }
    }

    public class UFConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return -1;

            var f = UFCollection.Estados.ToList();
            return f.IndexOf(f.First(x => x.Key == (string)value));

            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
