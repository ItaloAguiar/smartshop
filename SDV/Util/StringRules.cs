﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace SDV.Util
{
    public class StringToIntRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                int r = 0;
                if (int.TryParse((string)value, out r))
                    return new ValidationResult(true, null);
                else
                    return new ValidationResult(false, "Formato não reconhecido.");
            }
            catch
            {
                return new ValidationResult(false, "Formato não reconhecido.");
            }
        }
    }

    public class StringToDecimalRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                decimal r = 0;
                if (decimal.TryParse((string)value, out r))
                    return new ValidationResult(true, null);
                else
                    return new ValidationResult(false, "Formato não reconhecido.");
            }
            catch
            {
                return new ValidationResult(false, "Formato não reconhecido.");
            }
        }
    }

    public class RegexRule : ValidationRule
    {
        public string Pattern { get; set; } 
        public bool AllowNull { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null)
                return new ValidationResult(AllowNull, AllowNull ? null : "Campo obrigatório");

            Regex r = new Regex(Pattern);
            if(r.IsMatch((string)value))
            {
                return new ValidationResult(true, null);
            }
            else
            {
                return new ValidationResult(false, "Formato não reconhecido");
            }
        }
    }

    public class StringToCPFOrCNPJRule : ValidationRule
    {
        public bool AllowNull { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if(value == null)
                return new ValidationResult(AllowNull, AllowNull ? null : "Campo obrigatório");

            try
            {
                long r = 0;

                if (long.TryParse((string)value, out r) && IsCnpj((string)value) || IsCpf((string)value))
                    return new ValidationResult(true, null);
                else
                    return new ValidationResult(false, "Número inválido");
            }
            catch
            {
                return new ValidationResult(false, "Número inválido");
            }

        }

        private bool IsCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }

        private bool IsCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }
    }
}
