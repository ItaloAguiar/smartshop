﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDV.Util
{
    public static class Sounds
    {
        static Sounds()
        {
            Asterisk = new Sound(System.AppDomain.CurrentDomain.BaseDirectory + "/Media/Asterisk.wav");
            Exclamation = new Sound(System.AppDomain.CurrentDomain.BaseDirectory + "/Media/Exclamation.wav");
            Question = new Sound(System.AppDomain.CurrentDomain.BaseDirectory + "/Media/Question.wav");
            Error = new Sound(System.AppDomain.CurrentDomain.BaseDirectory + "/Media/Exclamation.wav");
            Popup = new Sound(System.AppDomain.CurrentDomain.BaseDirectory + "/Media/Popup.wav");
            Welcome = new Sound(System.AppDomain.CurrentDomain.BaseDirectory + "/Media/Welcome.wav");
        }
        public static Sound Asterisk { get; set; }
        public static Sound Exclamation { get; set; }
        public static Sound Question { get; set; }
        public static Sound Error { get; set; }
        public static Sound Popup { get; set; }
        public static Sound Welcome { get; set; }

    }
    public class Sound
    {
        public Sound(string Source)
        {
            player = new System.Media.SoundPlayer(Source.ToString());
            player.LoadAsync();
            this.Source = Source;
        }

        public void Play()
        {
            player.Play();
        }

        public string Source { get; set; }
        protected System.Media.SoundPlayer player;        
    }
}
