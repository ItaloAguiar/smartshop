//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SDV.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public partial class Produto : INotifyPropertyChanged
    {
        private byte[] _imagem;
        private Unidade _unidade;
        private string _nome;
        private string _marca;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Produto()
        {
            this.CompraProdutos = new HashSet<CompraProdutos>();
            this.VendaProdutos = new HashSet<VendaProdutos>();
        }

        public int Id { get; set; }
        public Nullable<int> CodigoBarras { get; set; }
        public string Nome { get => _nome; set => _nome = value.Trim(); }
        public string Descricao { get; set; }
        public decimal Preco { get; set; }
        public int Estoque { get; set; }
        public int EstoqueMinimo { get; set; }
        public string IdUnidade { get; set; }
        public int Desconto { get; set; }
        public Nullable<decimal> PrecoCusto { get; set; }
        public int Comissao { get; set; }
        public Nullable<int> NCM { get; set; }
        public byte[] Imagem
        {
            get => _imagem;
            set
            {
                _imagem = value;
                OnPropertyChanged("Imagem");
            }
        }
        public string Marca { get => _marca; set => _marca = value.Trim(); }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompraProdutos> CompraProdutos { get; set; }
        public virtual Unidade Unidade
        {
            get => _unidade;
            set
            {
                _unidade = value;
                OnPropertyChanged("Unidade");
            }
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VendaProdutos> VendaProdutos { get; set; }


        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
