//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SDV.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class FormaPagamento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FormaPagamento()
        {
            this.Pagamentos = new HashSet<Pagamento>();
        }
    
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Parcelado { get; set; }
        public Nullable<int> MaxParcelas { get; set; }
        public bool PermitePrazo { get; set; }
        public bool GeraBoleto { get; set; }
        public bool AbreGaveta { get; set; }
        public byte[] Icone { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pagamento> Pagamentos { get; set; }
    }
}
