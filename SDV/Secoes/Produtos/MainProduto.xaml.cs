﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Produtos.Relatorios
{
    /// <summary>
    /// Interaction logic for MainProduto.xaml
    /// </summary>
    public partial class MainProduto : UserControl
    {
        public MainProduto()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //preco.TryGetDouble();
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro ao salvar o produto. Verifique o campo selecionado. (Valor inválido!)");
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem box = (sender as ListBox).SelectedItem as ListBoxItem;

            if (box != null)

                switch (box.Tag.ToString())
                {
                    case "0":
                        content.Content = (new SDV.Secoes.Produtos.EditarProduto());
                        break;
                    case "1":
                        content.Content = (new SDV.Secoes.Produtos.PesquisaProduto());
                        break;
                    case "2":
                        //content.ShowPage(new SDV.Secoes.Produtos.EditarProduto());
                        break;
                    default:
                        break;
                }
        }
    }
}
