﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Secoes.Produtos
{
    /// <summary>
    /// Interaction logic for PesquisaProduto.xaml
    /// </summary>
    public partial class PesquisaProduto : UserControl
    {
        public PesquisaProduto()
        {
            InitializeComponent();
        }
        EF.DBModel ef = new EF.DBModel();
        private void CustomTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            listView1.ItemsSource = ef.Produtoes.Where(p => p.Nome.Contains(txt1.Text)).ToList();
        }

        private void listView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listView1.SelectedItem != null)
            {
                ((ContentControl)Parent).Content = new EditarProduto(ef, listView1.SelectedItem as EF.Produto);
            }
        }
    }
}
