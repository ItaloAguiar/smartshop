﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Secoes.Produtos
{
    /// <summary>
    /// Interaction logic for EditarProduto.xaml
    /// </summary>
    public partial class EditarProduto : UserControl
    {
        public EditarProduto(EF.DBModel model, EF.Produto arg)
        {
            InitializeComponent();


            modoedicao = ModoEdicao.Editar;
            Entity = model;
            var u = Entity.Unidade.ToList();
            unid.ItemsSource = u;
            Produto = arg;
            DataContext = Produto;
        }
        public EditarProduto()
        {
            InitializeComponent();


            modoedicao = ModoEdicao.Novo;
            Entity = new EF.DBModel();
            var u = Entity.Unidade.ToList();
            unid.ItemsSource = u;
            Produto = new EF.Produto();
            DataContext = Produto;
        }
        
        private EF.Produto Produto;
        private ModoEdicao modoedicao;
        private EF.DBModel Entity;


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            switch (modoedicao)
            {
                case ModoEdicao.Novo:
                    Entity.Produtoes.Add(Produto);
                    Entity.SaveChanges();
                    break;
                case ModoEdicao.Editar:
                    Entity.SaveChanges();
                    break;
            }
            ((ContentControl)Parent).Content = new Default();
        }

        private void EscolherImagem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                try
                {
                    var img = System.Drawing.Image.FromStream(ofd.OpenFile());
                    var image = CompressImage(img, 50);

                    Produto.Imagem = image;
                }
                catch
                {
                    MessageBox.Show("Erro ao processar imagem");
                }

            }
        }

        private void Image_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            EscolherImagem_Click(sender, e);
        }

        private byte[] CompressImage(System.Drawing.Image sourceImage, int imageQuality)
        {
            try
            {
                //Create an ImageCodecInfo-object for the codec information
                ImageCodecInfo jpegCodec = null;

                //Set quality factor for compression
                EncoderParameter imageQualitysParameter = new EncoderParameter(
                            System.Drawing.Imaging.Encoder.Quality, imageQuality);

                //List all avaible codecs (system wide)
                ImageCodecInfo[] alleCodecs = ImageCodecInfo.GetImageEncoders();

                EncoderParameters codecParameter = new EncoderParameters(1);
                codecParameter.Param[0] = imageQualitysParameter;

                //Find and choose JPEG codec
                for (int i = 0; i < alleCodecs.Length; i++)
                {
                    if (alleCodecs[i].MimeType == "image/jpeg")
                    {
                        jpegCodec = alleCodecs[i];
                        break;
                    }
                }

                //Save compressed image
                using (var ms = new MemoryStream())
                {
                    sourceImage.Save(ms, jpegCodec, codecParameter);
                    return ms.ToArray();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void PesquinaNaWeb_Click(object sender, RoutedEventArgs e)
        {
            Produto.Imagem = Util.WebImageSearcher.FromText(nome.Text);
        }
    }
}
