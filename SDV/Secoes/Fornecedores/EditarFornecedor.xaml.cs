﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Secoes.Fornecedores
{
    /// <summary>
    /// Interaction logic for EditarProduto.xaml
    /// </summary>
    public partial class EditarFornecedor : UserControl
    {
        public EditarFornecedor(EF.DBModel model, EF.Fornecedor arg)
        {
            InitializeComponent();

            txtCNPJ.IsEnabled = false;

            modoedicao = ModoEdicao.Editar;
            Entity = model;
            Fornecedor = arg;
            DataContext = Fornecedor;
        }
        public EditarFornecedor()
        {
            InitializeComponent();


            modoedicao = ModoEdicao.Novo;
            Entity = new EF.DBModel();
            Fornecedor = new EF.Fornecedor();
            Fornecedor.Endereco = new EF.Endereco();
            DataContext = Fornecedor;
        }
        
        private EF.Fornecedor Fornecedor;
        private ModoEdicao modoedicao;
        private EF.DBModel Entity;


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            switch (modoedicao)
            {
                case ModoEdicao.Novo:
                    Entity.Fornecedores.Add(Fornecedor);
                    Entity.SaveChanges();
                    break;
                case ModoEdicao.Editar:
                    Entity.SaveChanges();
                    break;
            }
            ((ContentControl)Parent).Content = new Default();
        }

    }
}
