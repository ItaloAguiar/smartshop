﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Secoes.Fornecedores
{
    /// <summary>
    /// Interaction logic for PesquisaProduto.xaml
    /// </summary>
    public partial class PesquisaFornecedor : UserControl
    {
        public PesquisaFornecedor()
        {
            InitializeComponent();
        }
        EF.DBModel ef = new EF.DBModel();
        private void CustomTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            listView1.ItemsSource = ef.Fornecedores.Where(p => p.RazaoSocial.Contains(txt1.Text)).ToList();
        }

        private void listView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listView1.SelectedItem != null)
            {
                ((ContentControl)Parent).Content = new EditarFornecedor(ef, listView1.SelectedItem as EF.Fornecedor);
            }
        }
    }
}
