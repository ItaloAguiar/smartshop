﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Secoes
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : UserControl
    {
        public HomePage(string username)
        {
            InitializeComponent();

            //EF.LocalDatabaseEntities c = new EF.LocalDatabaseEntities();


            //var v = c.Database.SqlQuery<DBDataReturned>("SELECT 'Valor' K, SUM(Total) V FROM contac_vendas WHERE Status = 1;").ToList()[0].V;
            //TotalReceber.Text = string.Format("{0:C}", v);

            ////c1.ItemsSource = c.Database.SqlQuery<DBDataReturned>("select REPLACE(REPLACE(Status,'1','A Receber'),'0','Recebido') K, SUM(Total) V from contac_vendas GROUP BY Status;").ToObservableCollection();

            ////c2.ItemsSource = c.Database.SqlQuery<DBDataReturned>("Select MonthName(Data) K, SUM(Total) V FROM contac_vendas WHERE year(Data) = @p0 GROUP BY month(Data);", DateTime.Now.Year).ToObservableCollection();

            ////string dc3 = "select \"Inadimplentes\" K, (count(DISTINCT Cliente)/(SELECT count(*) from contac_cli))*100 " +
            ////"AS V from contac_vendas where Status=1 AND Cliente in(Select ID from contac_cli) UNION " +
            ////"select \"Em dia\" K, (count(*)/(SELECT count(*) from contac_cli))*100 as V from contac_cli " +
            ////"where ID not in(Select Cliente from contac_vendas where Status=1);";
            ////var d = c.Database.SqlQuery<DBDataReturned>(dc3).ToObservableCollection();
            ////c3.ItemsSource = d;

            ////Clientes devedores/total de clientes
            //string clientes = "SELECT COUNT(*) K,T V FROM (SELECT * FROM contac.contac_vendas WHERE Status=1 GROUP BY Cliente)SUB JOIN (SELECT count(*) T FROM contac.contac_cli)T;";
            //var cli = c.Database.SqlQuery<DBDataReturned>(clientes).ToObservableCollection();
            //ClientesReceber.Text = cli[0].K;
            //ClientesTotal.Text = cli[0].V.ToString();

            //if (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 12)
            //{
            //    wlcome.Text = string.Format("Bom dia {0},", username);
            //}
            //else if (DateTime.Now.Hour >= 12 && DateTime.Now.Hour < 18)
            //{
            //    wlcome.Text = string.Format("Boa tarde {0},", username);
            //}
            //else
            //{
            //    wlcome.Text = string.Format("Boa noite {0},", username);
            //}
            //c.Database.Connection.Close();
            //c.Dispose();
            PointLabel = chartPoint =>
                string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation);

            SeriesCollection = new SeriesCollection
            {
                new RowSeries
                {
                    Title = "2015",
                    Values = new ChartValues<double> { 10, 50 }
                }
            };

            //adding series will update and animate the chart automatically
            SeriesCollection.Add(new RowSeries
            {
                Title = "2016",
                Values = new ChartValues<double> { 11, 56 }
            });

            //also adding values updates and animates the chart automatically
            SeriesCollection[1].Values.Add(48d);

            Labels = new[] { "Maria", "Susan" };
            Formatter = value => value.ToString("N");









            SeriesCollection2 = new SeriesCollection
            {
                new StackedAreaSeries
                {
                    Title = "Africa",
                    Values = new ChartValues<DateTimePoint>
                    {
                        new DateTimePoint(new DateTime(1950, 1, 1), .228),
                        new DateTimePoint(new DateTime(1960, 1, 1), .285),
                        new DateTimePoint(new DateTime(1970, 1, 1), .366),
                        new DateTimePoint(new DateTime(1980, 1, 1), .478),
                        new DateTimePoint(new DateTime(1990, 1, 1), .629),
                        new DateTimePoint(new DateTime(2000, 1, 1), .808),
                        new DateTimePoint(new DateTime(2010, 1, 1), 1.031),
                        new DateTimePoint(new DateTime(2013, 1, 1), 1.110)
                    },
                    LineSmoothness = 0
                },
                new StackedAreaSeries
                {
                    Title = "N & S America",
                    Values = new ChartValues<DateTimePoint>
                    {
                        new DateTimePoint(new DateTime(1950, 1, 1), .339),
                        new DateTimePoint(new DateTime(1960, 1, 1), .424),
                        new DateTimePoint(new DateTime(1970, 1, 1), .519),
                        new DateTimePoint(new DateTime(1980, 1, 1), .618),
                        new DateTimePoint(new DateTime(1990, 1, 1), .727),
                        new DateTimePoint(new DateTime(2000, 1, 1), .841),
                        new DateTimePoint(new DateTime(2010, 1, 1), .942),
                        new DateTimePoint(new DateTime(2013, 1, 1), .972)
                    },
                    LineSmoothness = 0
                },
                new StackedAreaSeries
                {
                    Title = "Asia",
                    Values = new ChartValues<DateTimePoint>
                    {
                        new DateTimePoint(new DateTime(1950, 1, 1), 1.395),
                        new DateTimePoint(new DateTime(1960, 1, 1), 1.694),
                        new DateTimePoint(new DateTime(1970, 1, 1), 2.128),
                        new DateTimePoint(new DateTime(1980, 1, 1), 2.634),
                        new DateTimePoint(new DateTime(1990, 1, 1), 3.213),
                        new DateTimePoint(new DateTime(2000, 1, 1), 3.717),
                        new DateTimePoint(new DateTime(2010, 1, 1), 4.165),
                        new DateTimePoint(new DateTime(2013, 1, 1), 4.298)
                    },
                    LineSmoothness = 0
                },
                new StackedAreaSeries
                {
                    Title = "Europe",
                    Values = new ChartValues<DateTimePoint>
                    {
                        new DateTimePoint(new DateTime(1950, 1, 1), .549),
                        new DateTimePoint(new DateTime(1960, 1, 1), .605),
                        new DateTimePoint(new DateTime(1970, 1, 1), .657),
                        new DateTimePoint(new DateTime(1980, 1, 1), .694),
                        new DateTimePoint(new DateTime(1990, 1, 1), .723),
                        new DateTimePoint(new DateTime(2000, 1, 1), .729),
                        new DateTimePoint(new DateTime(2010, 1, 1), .740),
                        new DateTimePoint(new DateTime(2013, 1, 1), .742)
                    },
                    LineSmoothness = 0
                }
            };

            XFormatter = val => new DateTime((long)val).ToString("yyyy");
            YFormatter = val => val.ToString("N") + " M";





            DataContext = this;
        }
        public Func<ChartPoint, string> PointLabel { get; set; }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> Formatter { get; set; }

        public SeriesCollection SeriesCollection2 { get; set; }
        public Func<double, string> XFormatter { get; set; }
        public Func<double, string> YFormatter { get; set; }

        private void PieChart_DataClick(object sender, ChartPoint chartpoint)
        {
            var chart = (LiveCharts.Wpf.PieChart)chartpoint.ChartView;

            //clear selected slice.
            foreach (PieSeries series in chart.Series)
                series.PushOut = 0;

            var selectedSeries = (PieSeries)chartpoint.SeriesView;
            selectedSeries.PushOut = 8;
        }
    }
    public static class LinqExtensions
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> _LinqResult)
        {
            return new ObservableCollection<T>(_LinqResult);
        }
    }
    public class DBDataReturned
    {
        public string K { get; set; }
        public double V { get; set; }
    }
}
