﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Secoes.Clientes
{
    /// <summary>
    /// Interaction logic for EditarCliente.xaml
    /// </summary>
    public partial class EditarCliente : UserControl
    {
        public EditarCliente()
        {
            InitializeComponent();

            Cliente = new EF.Cliente() { PessoaFisica = true , Situacao=true};            
            DataContext = Cliente;
            enderecos.ItemsGenerated += (s, a) =>
            {
                enderecos.OpenAccordionItem(0);
            };
            DB = new EF.DBModel();
            Modo = ModoEdicao.Novo;
        }

        public EditarCliente(EF.Cliente cli,EF.DBModel db)
        {
            InitializeComponent();

            Cliente = cli;
            DataContext = Cliente;
            enderecos.ItemsGenerated += (s, a) =>
            {
                enderecos.OpenAccordionItem(0);
            };
            DB = db;
            Modo = ModoEdicao.Editar;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (((TabControl)sender).SelectedIndex)
            {
                case 1:
                    add_enderecoBtn.Visibility = Visibility.Visible;
                    break;
                default:
                    add_enderecoBtn.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        public EF.Cliente Cliente { get; set; }
        private EF.DBModel DB;
        private ModoEdicao Modo;

        private void add_enderecoBtn_Click(object sender, RoutedEventArgs e)
        {
            

            Cliente.Enderecos.Add(new EF.Endereco());
            enderecos.ItemsSource = null;
            enderecos.ItemsSource = Cliente.Enderecos;
            enderecos.CloseAllAccordionItems();
            enderecos.OpenAccordionItem(Cliente.Enderecos.Count - 1, true);
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (Util.FormValidator.IsValid(this))
            {
                switch (Modo)
                {
                    case ModoEdicao.Novo:
                        DB.Cliente.Add(Cliente);                        
                        break;
                    case ModoEdicao.Editar:
                        break;                        
                }
                DB.SaveChanges();
                ((ContentControl)Parent).Content = new Default();
            }
            else
            {
                Util.Sounds.Error.Play();
            }
        }
    }
}
