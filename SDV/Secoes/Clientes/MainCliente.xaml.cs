﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Secoes.Clientes
{
    /// <summary>
    /// Interaction logic for MainCliente.xaml
    /// </summary>
    public partial class MainCliente : UserControl
    {
        public MainCliente()
        {
            InitializeComponent();
            content.Content = (new SDV.Secoes.Clientes.Default());
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem box = (sender as ListBox).SelectedItem as ListBoxItem;

            if (box != null)

                switch (box.Tag.ToString())
                {
                    case "0":
                        content.Content = (new SDV.Secoes.Clientes.EditarCliente());
                        break;
                    case "1":
                        content.Content = (new SDV.Secoes.Clientes.PesquisarCliente());
                        break;
                    case "2":
                        content.Content = (new SDV.Secoes.Clientes.EditarCliente());
                        break;
                    default:
                        break;
                }
        }
        public void SetContent(UserControl e)
        {
            content.Content = (e);
        }
    }
}
