﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SDV.Secoes.Clientes
{
    /// <summary>
    /// Interaction logic for PesquisarCliente.xaml
    /// </summary>
    public partial class PesquisarCliente : UserControl
    {
        public PesquisarCliente()
        {
            InitializeComponent();
            DB = new EF.DBModel();
        }

        private EF.DBModel DB;
        
        private void CustomTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            string s = txt1.Text;
            if (!string.IsNullOrEmpty(s) && s.Length % 2 != 0)
            {
                var sql = DB.Cliente.Where(p => p.Nome.Contains(s)|| p.NomeFantasia.Contains(s));

                listView1.ItemsSource = sql.ToList();

            }
        }

        private void listView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listView1.SelectedItem != null)
            {
                ((ContentControl)Parent).Content = new EditarCliente(listView1.SelectedItem as EF.Cliente, DB);
            }
        }
    }
}
