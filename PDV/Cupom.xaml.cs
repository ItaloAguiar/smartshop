﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PDV.Controls
{
    /// <summary>
    /// Interaction logic for Cupom.xaml
    /// </summary>
    public partial class Cupom : UserControl
    {
        public Cupom()
        {
            InitializeComponent();


            
            dt.Interval = new TimeSpan(0, 0, 1);
            int i = 0;
            Random rnd = new Random();
            dt.Tick += (s, k) =>
            {
                Produto a = new Produto();
                a.Nome = "PRODUTO PRODUTO";
                a.Codigo = rnd.Next(100000,500000);
                a.ValorTotal = 80.80f;
                a.ValorUnitario = 80.80f;
                a.Quantidade = i++;

                Add(a);
            };
            dt.Start();

        }

        ~Cupom()
        {
            dt.Stop();
        }

        DispatcherTimer dt = new DispatcherTimer();

        public void Add(Produto p)
        {
            list.Items.Add(p);
            list.ScrollIntoView(p);

           // Console.Beep();
        }
    }
    public class DesignItem : List<Produto>
    {
        public DesignItem()
        {
            for (int i = 0; i < 10; i++)
            {
                Produto a = new Produto();
                a.Nome = "PRODUTO PRODUTO";
                a.Codigo = 1241241243;
                a.ValorTotal = 80.80f;
                a.ValorUnitario = 80.80f;
                a.Quantidade = 1;
                this.Add(a);
            }
           
        }
    }
    public class Produto
    {
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public long Codigo { get; set; }
        public float ValorUnitario { get; set; }
        public float ValorTotal { get; set; }

        //public static Produto FromDatabase(Database.Produto p)
        //{
        //    Produto pr = new Produto();
        //    pr.Codigo = (long)p.Barcode;
        //    pr.Nome = p.Nome;
        //    return pr;
        //}
    }
}
