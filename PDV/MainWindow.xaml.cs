﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PDV
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Opacity = 0;
            TextCompositionManager.AddTextInputHandler(this, new TextCompositionEventHandler(OnTextComposition));
            this.Closing += (sender, e) => 
            {
                var msg = MessageBox.Show("Deseja realmente sair? Qualquer venda não finalizada será descartada.", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question);
                e.Cancel = msg == MessageBoxResult.No;                
            };
            this.KeyDown += (sender, e) =>
            {
                if (e.Key == Key.Escape) Close();
            };

            //(App.Current as App).InstalledPrinters.First().Value.Receipt.Open("Deposito Soares LTDA", "Rua Eulampio de Assis Moraes", "Acucena", "MG", "35150-000", "(33)3298-????", "12344567890", "MG12345678");
        }

        private void OnTextComposition(object sender, TextCompositionEventArgs e)
        {
            //enter
            //if ("\r" == e.Text)
            //{
            //    prod.Text = string.Empty;
            //    if(list.SelectedItem != null)
            //    {
            //        var c = PDV.Controls.Produto.FromDatabase((Database.Produto)list.SelectedItem);
            //        cupom.Add(c);
            //        //list.Items.Clear();
            //        popup.IsOpen = false;
            //    }
            //}

            //if (!string.IsNullOrEmpty(e.Text.Trim()) && e.Text[0] >= 65 && e.Text[0] <= 90 || e.Text[0] == 32)
            //{
            //    prod.Text += e.Text;
            //    using (var d = new Database.SSHOPEntities())
            //    {
            //        d.Database.Log = s => System.Diagnostics.Debug.Write(s);
            //        var q = prod.Text;
            //        var result = from p in d.Produtos where p.Nome.Contains(q) select p;
            //        var items = result.Take(8).ToList();

            //        popup.IsOpen = items.Count > 0;
            //        list.ItemsSource = items;
            //    }
            //}
            //else if (IsTextAllowed(e.Text) && prod.Text.Length < 14)
            //    prod.Text += e.Text;

            //if ("\b" == e.Text)
            //    if (prod.Text.Length > 0)
            //        prod.Text = prod.Text.Substring(0, prod.Text.Length - 1);

        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            (sender as MediaElement).Position = new TimeSpan(0, 0, 1);
            (sender as MediaElement).Play();
        }

       
    }
}
