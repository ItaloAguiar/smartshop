﻿using PrinterExtensibility;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace PDV
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        [ImportMany(typeof(IPrinter))]
        public IEnumerable<Lazy<IPrinter>> InstalledPrinters { get; set; }
                
    }
}
