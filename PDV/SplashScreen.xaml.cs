﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PDV
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        public SplashScreen()
        {
            InitializeComponent();
            Initialize();
        }
        public async void Initialize()
        {
            CurrentStatus = "Aguarde, carregando...";
            await Task.Delay(2000);
            CurrentStatus = "Carregando Configurações...";
            await Task.Delay(2000);
            CurrentStatus = "Drivers de impressão...";

            try
            {
                string path = System.AppDomain.CurrentDomain.BaseDirectory + "Plugins\\Printer";
                var catalog = new DirectoryCatalog(path);
                var container = new CompositionContainer(catalog);
                container.ComposeParts(App.Current as App);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
            }
            await Task.Delay(2000);
            MainWindow m = new MainWindow();
            Close();
            m.Show();
        }
        public string CurrentStatus
        {            
            set
            {
                status.Text = value;
            }
        }
    }
}
