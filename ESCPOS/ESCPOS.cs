﻿using PrinterExtensibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESCPOS
{
    public class ESCPOS : IPrinter
    {
        private DevolutionReceipt _devolutionReceipt;
        public IDevolutionReceipt DevolutionReceipt
        {
            get
            {
                if (_devolutionReceipt == null) _devolutionReceipt = new DevolutionReceipt();
                return _devolutionReceipt;
            }
        }

        public string Name
        {
            get
            {
                return "EPSON ESC/POS PRINTER";
            }
        }
        private Receipt _paymentReceipt;
        public IReceipt PaymentReceipt
        {
            get
            {
                if (_paymentReceipt == null) _paymentReceipt = new Receipt();
                return _paymentReceipt;
            }
        }
        private Receipt _receipt;
        public IReceipt Receipt
        {
            get
            {
                if (_receipt == null) _receipt = new Receipt();
                return _receipt;
            }
        }

        public PrinterStatus Status
        {
            get
            {
                return PrinterStatus.Unavailable;
            }
        }

        public void CutPaper()
        {
            throw new NotImplementedException();
        }

        public void OpenCashDrawer()
        {
            throw new NotImplementedException();
        }

        public void OpenSettings()
        {
            throw new NotImplementedException();
        }

        public void PaperFeed()
        {
            throw new NotImplementedException();
        }

        public Task PrintLine(string c)
        {
            throw new NotImplementedException();
        }
    }
    public static class Extensions
    {
        public static string FillEndsWith(this String str, char c, int maxSize)
        {
            string r = "";
            for (int i = 0; i < maxSize; i++)
            {
                if (i < str.Length)
                    r += str[i];
                else
                    r += c;
            }
            return r;
        }
        public static string FillStartsWith(this String str, char c, int maxSize)
        {
            string r = "";
            for (int i = 0; i < maxSize; i++)
            {
                if (i < str.Length)
                    r += c;
                else
                    r += str[i];
            }
            return r;
        }
        public static string DecodeEscCommands(this String str)
        {
            string aux = str;


            List<KeyValuePair<string, char>> keyCodes = new List<KeyValuePair<string, char>>();
            keyCodes.Add(new KeyValuePair<string, char>("[ESC]", (char)27));
            keyCodes.Add(new KeyValuePair<string, char>("[HT]", (char)9));
            keyCodes.Add(new KeyValuePair<string, char>("[LF]", (char)10));
            keyCodes.Add(new KeyValuePair<string, char>("[FF]", (char)12));
            keyCodes.Add(new KeyValuePair<string, char>("[CR]", (char)13));
            keyCodes.Add(new KeyValuePair<string, char>("[DLE]", (char)16));
            keyCodes.Add(new KeyValuePair<string, char>("[EOT]", (char)4));
            keyCodes.Add(new KeyValuePair<string, char>("[ENQ]", (char)5));
            keyCodes.Add(new KeyValuePair<string, char>("[DC4]", (char)20));
            keyCodes.Add(new KeyValuePair<string, char>("[SP]", (char)32));
            keyCodes.Add(new KeyValuePair<string, char>("[!]", (char)33));
            keyCodes.Add(new KeyValuePair<string, char>("[$]", (char)36));
            keyCodes.Add(new KeyValuePair<string, char>("[%]", (char)37));
            keyCodes.Add(new KeyValuePair<string, char>("[&]", (char)38));
            keyCodes.Add(new KeyValuePair<string, char>("[*]", (char)42));
            keyCodes.Add(new KeyValuePair<string, char>("[_]", (char)45));
            keyCodes.Add(new KeyValuePair<string, char>("[2]", (char)50));
            keyCodes.Add(new KeyValuePair<string, char>("[3]", (char)51));
            keyCodes.Add(new KeyValuePair<string, char>("[=]", (char)61));
            keyCodes.Add(new KeyValuePair<string, char>("[?]", (char)63));
            keyCodes.Add(new KeyValuePair<string, char>("[@]", (char)64));
            keyCodes.Add(new KeyValuePair<string, char>(@"[\]", (char)92));
            keyCodes.Add(new KeyValuePair<string, char>("[FS]", (char)28));
            keyCodes.Add(new KeyValuePair<string, char>("[GS]", (char)29));
            keyCodes.Add(new KeyValuePair<string, char>("[^]", (char)94));
            keyCodes.Add(new KeyValuePair<string, char>("[-]", (char)45));
            keyCodes.Add(new KeyValuePair<string, char>("[.]", (char)46));
            keyCodes.Add(new KeyValuePair<string, char>("[SYN]", (char)22));
            keyCodes.Add(new KeyValuePair<string, char>("[;]", (char)0));
            keyCodes.Add(new KeyValuePair<string, char>("[1]", (char)1));
            keyCodes.Add(new KeyValuePair<string, char>("[2]", (char)2));
            keyCodes.Add(new KeyValuePair<string, char>("[3]", (char)3));
            keyCodes.Add(new KeyValuePair<string, char>("[4]", (char)4));

            foreach (KeyValuePair<string, char> v in keyCodes)
            {
                aux = aux.Replace(v.Key, v.Value.ToString());
            }

            return aux;
        }
    }
}
