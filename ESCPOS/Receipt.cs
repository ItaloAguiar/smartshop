﻿using PrinterExtensibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ESCPOS
{
    public class Receipt : IReceipt
    {
        public virtual Task AddProduct(int i, int q, string p, double v, double t)
        {
            throw new NotImplementedException();
        }

        public virtual void Cancel()
        {
            throw new NotImplementedException();
        }

        public virtual void Close()
        {
            throw new NotImplementedException();
        }

        public virtual Task Open(string e, string a, string c, string u, string p, string phone, string cnpj, string ie)
        {
            return Task.Run(() =>
            {
                string esc = "[ESC]![;][ESC]a1\n" +
                            "{0}\n{1},{2}-{3} {4} TEL:{5}\nIE: {6}   CNPJ:{7}\n" +
                            "[ESC]a0-----------------------------------------\n" +
                            "{8}[HT]CCNF:000\n" +
                            "-----------------------------------------\n" +
                            "[ESC]a1[ESC]!8CUPOM NÃO FISCAL\n[ESC]a0[ESC]![;]\n" +
                            "COD DESCRIC. QTD. VL.UNIT(R$) VL.TOTAL(R$)\n" +
                            "------------------------------------------\n";
                var pk = string.Format(esc, e, a, c, u, p, phone, ie, cnpj, DateTime.Now.ToString("dd/MM/yyyy HH:ss")).DecodeEscCommands();

                if (string.IsNullOrEmpty(Settings.Default.PrinterName))
                {
                    PrintDialog dlg = new PrintDialog();

                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        Settings.Default.PrinterName = dlg.PrinterSettings.PrinterName;
                        Settings.Default.Save();
                    }
                }

                RawPrinter.RawPrinterHelper.SendStringToPrinter(Settings.Default.PrinterName, pk);

            });
        }

        public virtual Task PrintClient(string n, string a, string cpf)
        {
            throw new NotImplementedException();
        }
    }
}
