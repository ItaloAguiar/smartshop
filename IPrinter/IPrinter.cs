﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterExtensibility
{
    [InheritedExport(typeof(IPrinter))]
    public interface IPrinter
    {
        IReceipt Receipt { get; }
        IReceipt PaymentReceipt { get; }
        IDevolutionReceipt DevolutionReceipt { get; }       
        Task PrintLine(string c);
        void CutPaper();
        void PaperFeed();
        PrinterStatus Status { get; }
        string Name { get; }
        void OpenSettings();
        void OpenCashDrawer();
    }
    public enum PrinterStatus
    {
        Ok, Paper_Low, Error, Paper_Error, Out_Of_Paper, Off, Unavailable
    }
    public interface IReceipt
    {
        Task Open(string e, string a, string c, string u, string p, string phone, string cnpj, string ie);
        Task AddProduct(int i, int q, string p, double v, double t);
        void Cancel();
        Task PrintClient(string n, string a, string cpf);
        void Close();
    }
    public interface IDevolutionReceipt
    {
        Task Open(string e, string a, string c, string u, string p, string phone, string cnpj, string ie);
        Task Print(double v);
        Task PrintClient(string n, string a, string cpf);
        void Close();
        void Cancel();
    }
}
